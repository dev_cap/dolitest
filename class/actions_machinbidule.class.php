<?php
/* Copyright (C) 2021 SuperAdmin <admin@test.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    machinbidule/class/actions_machinbidule.class.php
 * \ingroup machinbidule
 * \brief   Example hook overload.
 *
 * Put detailed description here.
 */

/**
 * Class ActionsMachinBidule
 */
class ActionsMachinBidule
{
	/**
	 * @var DoliDB Database handler.
	 */
	public $db;

	/**
	 * @var string Error code (or message)
	 */
	public $error = '';

	/**
	 * @var array Errors
	 */
	public $errors = array();


	/**
	 * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
	 */
	public $results = array();

	/**
	 * @var string String displayed by executeHook() immediately after return
	 */
	public $resprints;


	/**
	 * Constructor
	 *
	 *  @param		DoliDB		$db      Database handler
	 */
	public function __construct($db)
	{
		$this->db = $db;
	}


	/**
	 * Execute action
	 *
	 * @param	array			$parameters		Array of parameters
	 * @param	CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param	string			$action      	'add', 'update', 'view'
	 * @return	int         					<0 if KO,
	 *                           				=0 if OK but we want to process standard actions too,
	 *                            				>0 if OK and we want to replace standard actions.
	 */
	public function getNomUrl($parameters, &$object, &$action)
	{
		global $db, $langs, $conf, $user;
		$this->resprints = '';
		return 0;
	}

	/**
	 * Overloading the doActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('thirdpartycard', 'propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			//header(dol_buildpath('/machinbidule/maPage.php'));
			//exit;
			//propal

			if ($action=='create' AND $parameters['currentcontext']=='propalcard') {

				if (isset($_GET['origin']) AND $_GET['origin']=='project' AND isset($_GET['originid']) AND $_GET['originid']>0) {
					// on a un projet
				}
				else {
					//var_dump($object);exit();
					//GETPOSTISSET('socid') ;
					$lib_fiiltre = '' ;
					if (GETPOSTISSET('socid') AND $_GET['socid'] > 0) {
						$lib_fiiltre = '&socid='.$_GET['socid'] ;

						/*$soc = new Societe($this->db);
						$result = $soc->fetch($_GET['socid']);
						if ($result < 0) {
							$this->errors[] = $soc->error;
							$error++;
						}
						if (empty($error)) {
							$lib_search = $soc->name ;
						}*/
					}

					setEventMessage("Vous devez choisir une affaire","warnings");
					header("Location: ".dol_buildpath('/projet/list.php',1)."?leftmenu=projets".$lib_fiiltre);
				}
			}
			if ($action=='confirm_closeas' AND $parameters['currentcontext']=='propalcard') {
				if (!(GETPOST('statut', 'int') > 0)) {
					setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv("CloseAs")), null, 'errors');
					$action = 'closeas';
				} else {
					// prevent browser refresh from closing proposal several times
					if ($object->statut == $object::STATUS_VALIDATED)
					{
						//var_dump($_GET);exit();

						$this->db->begin();

						foreach ($_GET as $key=>$val) {
							if (substr($key,0,7)=='propal_') {
								$idpropal = substr($key,7,1) ;
								if ($object->id==$idpropal) {

								}
								else {
									if ($val=='on') {
										$othersPropal = new Propal($this->db);
										$result = $othersPropal->fetch($idpropal);
										if ($result < 0) {
											$this->errors[] = $othersPropal->error;
											$error++;
										} else {
											//var_dump($idpropal);
											$result = $othersPropal->cloture($user, Propal::STATUS_NOTSIGNED, 'Fermer Auto');
											if ($result < 0) {
												$this->errors[] = $othersPropal->error;
												$error++;
											}
										}
									}
								}
							}
						}
						//exit();

						$result = $object->cloture($user, GETPOST('statut', 'int'), GETPOST('note_private', 'restricthtml'));
						if ($result < 0)
						{
							setEventMessages($object->error, $object->errors, 'errors');
							$error++;
						}

						if (!$error)
						{
							$this->db->commit();
						} else {
							$this->db->rollback();
						}
					}
				}

			}


			// Do what you want here...
			// You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the doMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
			foreach ($parameters['toselect'] as $objectid)
			{
				// Do action on each object id
			}
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the addMoreMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function addMoreMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter
		$disabled = 1;

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
			$this->resprints = '<option value="0"'.($disabled ? ' disabled="disabled"' : '').'>'.$langs->trans("MachinBiduleMassAction").'</option>';
		}

		if (!$error) {
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}



	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$object		   	Object output on PDF
	 * @param   string	$action     	'add', 'update', 'view'
	 * @return  int 		        	<0 if KO,
	 *                          		=0 if OK but we want to process standard actions too,
	 *  	                            >0 if OK and we want to replace standard actions.
	 */
	public function beforePDFCreation($parameters, &$object, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0; $deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
		}

		return $ret;
	}

	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$pdfhandler     PDF builder handler
	 * @param   string	$action         'add', 'update', 'view'
	 * @return  int 		            <0 if KO,
	 *                                  =0 if OK but we want to process standard actions too,
	 *                                  >0 if OK and we want to replace standard actions.
	 */
	public function afterPDFCreation($parameters, &$pdfhandler, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0; $deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {
			// do something only for the context 'somecontext1' or 'somecontext2'
		}

		return $ret;
	}



	/**
	 * Overloading the loadDataForCustomReports function : returns data to complete the customreport tool
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function loadDataForCustomReports($parameters, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$langs->load("machinbidule@machinbidule");

		$this->results = array();

		$head = array();
		$h = 0;

		if ($parameters['tabfamily'] == 'machinbidule') {
			$head[$h][0] = dol_buildpath('/module/index.php', 1);
			$head[$h][1] = $langs->trans("Home");
			$head[$h][2] = 'home';
			$h++;

			$this->results['title'] = $langs->trans("MachinBidule");
			$this->results['picto'] = 'machinbidule@machinbidule';
		}

		$head[$h][0] = 'customreports.php?objecttype='.$parameters['objecttype'].(empty($parameters['tabfamily']) ? '' : '&tabfamily='.$parameters['tabfamily']);
		$head[$h][1] = $langs->trans("CustomReports");
		$head[$h][2] = 'customreports';

		$this->results['head'] = $head;

		return 1;
	}



	/**
	 * Overloading the restrictedArea function : check permission on an object
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int 		      			  	<0 if KO,
	 *                          				=0 if OK but we want to process standard actions too,
	 *  	                            		>0 if OK and we want to replace standard actions.
	 */
	public function restrictedArea($parameters, &$action, $hookmanager)
	{
		global $user;

		if ($parameters['features'] == 'myobject') {
			if ($user->rights->machinbidule->myobject->read) {
				$this->results['result'] = 1;
				return 1;
			} else {
				$this->results['result'] = 0;
				return 1;
			}
		}

		return 0;
	}

	public function formConfirm($parameters, &$object, &$action, $hookmanager) {
		global $langs;
		//$parameters['formConfirm'];
		//propalcard

		$form = new Form($this->db);

		if (in_array($parameters['currentcontext'], array('propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			//var_dump($action);
			//var_dump($parameters);exit();
			if ($action == 'closeas')
			{
				$nb_propal_plus = 0 ;
				//Form to close proposal (signed or not)
				$formquestion = array(
					array('type' => 'select', 'name' => 'statut', 'label' => '<span class="fieldrequired">'.$langs->trans("CloseAs").'</span>', 'values' => array($object::STATUS_SIGNED => $object->LibStatut($object::STATUS_SIGNED), $object::STATUS_NOTSIGNED => $object->LibStatut($object::STATUS_NOTSIGNED))),
					array('type' => 'text', 'name' => 'note_private', 'label' => $langs->trans("Note"), 'value' => '')				// Field to complete private note (not replace)
					//array('type' => 'text', 'name' => 'note_private_2', 'label' => $langs->trans("Note"), 'value' => 't')				// Field to complete private note (not replace)
				);
				require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
				$myproject = new Project($this->db);
				$result = $myproject->fetch($object->fk_project);
				if ($result < 0) {
					$this->errors[] = $myproject->error;
					$error++;
				}

				if (empty($error)) {
					$elementarray = $myproject->get_element_list('propal', 'propal', 'datep', '', '',  'fk_projet');
					if (!is_array($elementarray) && $elementarray < 0) {
						$this->errors[] = $this->db->lasterror;
						$error++;
					} else {
						$Tid_propal_check = array() ;
						foreach ($_GET as $key=>$val) {
							if (substr($key, 0, 7) == 'propal_') {
								$Tid_propal_check[substr($key, 7, 1)] = $val;
							}
						}
						foreach ($elementarray as $key=>$data) {
							$tmp = explode('_', $data);
							$idofelement = $tmp[0];
							if ($object->id==$idofelement) { // current propal

							}
							else {
								$othersPropal = new Propal($this->db);
								$result = $othersPropal->fetch($idofelement);
								if ($result < 0) {
									$this->errors[] = $othersPropal->error;
									$error++;
								} else {
									$checked = 1 ;
									//var_dump($Tid_propal_check);exit();
									if (isset($Tid_propal_check[$othersPropal->id]) AND $Tid_propal_check[$othersPropal->id]=='') {
										$checked = 0 ;
									}

									if ($nb_propal_plus==0) {
										$formquestion[] = array('type' => 'other', 'label' => "", 'value' => "<hr>Sélectionner les éléments à cloturer automatiquement", "tdclass"=>"tagtdmax") ;
									}

									//$formquestion[] = array('type' => 'text', 'name' => 'propal_'.$idofelement, 'label' => $langs->trans("propal"), 'value' => $idofelement) ;
									$formquestion[] = array('type' => 'checkbox', 'name' => 'propal_'.$othersPropal->id, 'label' => $othersPropal->ref.' - '.$othersPropal->ref_client.' - '.$othersPropal->statut, 'value' => $checked, 'morecss'=>'class="propal_sign_plus"') ;
									$nb_propal_plus++ ;
								}
							}
						}
					}
				}

				/*if ($nb_propal_plus>0) {
					?>
<script language="javascript">
	//selct on statut
	$(document).ready(function () {
		$("#dialog-confirm #statut")
	});
</script>
					<?php
				}*/

				if (!empty($conf->notification->enabled))
				{
					require_once DOL_DOCUMENT_ROOT.'/core/class/notify.class.php';
					$notify = new Notify($this->db);
					$formquestion = array_merge($formquestion, array(
						array('type' => 'onecolumn', 'value' => $notify->confirmMessage('PROPAL_CLOSE_SIGNED', $object->socid, $object)),
					));
				}

				$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('SetAcceptedRefused'), $text, 'confirm_closeas', $formquestion, '', 1, 350);

				$this->resprints =  $formconfirm ;
				return 1;
			}
		}
	}

	public function completeListOfReferent($parameters, &$object, &$action, $hookmanager) {
		global $langs;
		if (in_array($parameters['currentcontext'], array('projectOverview')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			//var_dump($parameters);exit();
			$out = "\n";
			$out .= '
<script type="text/javascript">
$(document).ready(function () {
	$(".titre").each(function( index ) {
	    if ($( this ).text()=="Bénéfice") {
			$( this ).parent().parent().addClass("hidden-search-result");
			$( this ).parent().parent().next().addClass("hidden-search-result");
			console.log( index + ": '.$langs->trans("Profit").' : " + $( this ).text() );

	    }
	});
});
</script>';
			$out .= "\n";
			print $out;
		}
	}

	public function printCommonFooter($parameters, &$object, &$action, $hookmanager) {
		global $langs;
		if (in_array($parameters['currentcontext'], array('propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{

			if ($_GET['action'] == 'create')
			{
				if (isset($_GET['origin']) AND $_GET['origin']=='project' AND isset($_GET['originid']) AND $_GET['originid']>0) {
					// on a un projet
				}
				else {
					/*$form = new Form($this->db);
					$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"], $langs->trans('SetAcceptedRefused'), $text, 'confirm_closeas', $formquestion, '', 1, 350);
					print $formconfirm ;*/
				}
				/*var_dump($_GET);exit();
				foreach ($_GET as $key=>$val) {
					if ($key=='')
					$_GET['socid'] ;
					$_GET['origin']=='project' ;
					$_GET['originid'] ;

				}*/
			}
		}

		if (in_array($parameters['currentcontext'], array('thirdpartycard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			$out = "\n";
			$out .= '
			<script type="text/javascript">
				$(document).ready(function () {
				    $("#zipcode").parent().parent().children("td:contains(\''.$langs->trans('Zip').'\')").addClass("fieldrequired");
				   // $("#zipcode").attr(\'required\',\'required\');
				    $("#town").parent().parent().children("td:contains(\''.$langs->trans('Town').'\')").addClass("fieldrequired");
				    $("#address").parent().parent().children("td:contains(\''.$langs->trans('Adress').'\')").addClass("fieldrequired");

				    $("#custcats").parent().parent().children("td:contains(\'Tag\')").addClass("fieldrequired");
				    //Todo Check how to do
				    //$("#custcats").attr(\'required\',\'required\');
				});
			</script>';
			$out .= "\n";
			print $out;
		}
	}

	/* Add here any other hooked methods... */
}
