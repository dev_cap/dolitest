-- Copyright (C) ---Put here your own copyright and developer email---
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see https://www.gnu.org/licenses/.


-- BEGIN MODULEBUILDER INDEXES
ALTER TABLE llx_machinbidule_livre ADD INDEX idx_machinbidule_livre_rowid (rowid);
ALTER TABLE llx_machinbidule_livre ADD INDEX idx_machinbidule_livre_ref (ref);
ALTER TABLE llx_machinbidule_livre ADD CONSTRAINT llx_machinbidule_livre_fk_socpeople_author FOREIGN KEY (fk_socpeople_author) REFERENCES llx_socpeople(rowid);
ALTER TABLE llx_machinbidule_livre ADD CONSTRAINT llx_machinbidule_livre_fk_genre FOREIGN KEY (fk_genre) REFERENCES llx_machinbidule_c_livre_genre(rowid);
ALTER TABLE llx_machinbidule_livre ADD CONSTRAINT llx_machinbidule_livre_fk_sub_genre FOREIGN KEY (fk_sub_genre) REFERENCES llx_machinbidule_c_livre_genre(rowid);
ALTER TABLE llx_machinbidule_livre ADD CONSTRAINT llx_machinbidule_livre_fk_user_creat FOREIGN KEY (fk_user_creat) REFERENCES llx_user(rowid);
ALTER TABLE llx_machinbidule_livre ADD INDEX idx_machinbidule_livre_status (status);
-- END MODULEBUILDER INDEXES

--ALTER TABLE llx_machinbidule_livre ADD UNIQUE INDEX uk_machinbidule_livre_fieldxy(fieldx, fieldy);

--ALTER TABLE llx_machinbidule_livre ADD CONSTRAINT llx_machinbidule_livre_fk_field FOREIGN KEY (fk_field) REFERENCES llx_machinbidule_myotherobject(rowid);

