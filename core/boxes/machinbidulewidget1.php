<?php
/* Copyright (C) 2004-2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2018-2020  Frédéric France     <frederic.france@netlogic.fr>
 * Copyright (C) 2021 Firstname SuperAdminName <bidon@mydomain.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    machinbidule/core/boxes/machinbidulewidget1.php
 * \ingroup machinbidule
 * \brief   Widget provided by MachinBidule
 *
 * Put detailed description here.
 */

include_once DOL_DOCUMENT_ROOT."/core/boxes/modules_boxes.php";


/**
 * Class to manage the box
 *
 * Warning: for the box to be detected correctly by dolibarr,
 * the filename should be the lowercase classname
 */
class machinbidulewidget1 extends ModeleBoxes
{
	/**
	 * @var string Alphanumeric ID. Populated by the constructor.
	 */
	public $boxcode = "machinbidulebox";

	/**
	 * @var string Box icon (in configuration page)
	 * Automatically calls the icon named with the corresponding "object_" prefix
	 */
	public $boximg = "machinbidule@machinbidule";

	/**
	 * @var string Box label (in configuration page)
	 */
	public $boxlabel;

	/**
	 * @var string[] Module dependencies
	 */
	public $depends = array('machinbidule');

	/**
	 * @var DoliDb Database handler
	 */
	public $db;

	/**
	 * @var mixed More parameters
	 */
	public $param;

	/**
	 * @var array Header informations. Usually created at runtime by loadBox().
	 */
	public $info_box_head = array();

	/**
	 * @var array Contents informations. Usually created at runtime by loadBox().
	 */
	public $info_box_contents = array();

	/**
	 * Constructor
	 *
	 * @param DoliDB $db Database handler
	 * @param string $param More parameters
	 */
	public function __construct(DoliDB $db, $param = '')
	{
		global $user, $conf, $langs;
		$langs->load("boxes");
		$langs->load('machinbidule@machinbidule');

		parent::__construct($db, $param);

		$this->boxlabel = $langs->transnoentitiesnoconv("MyWidget");

		$this->param = $param;

		//$this->enabled = $conf->global->FEATURES_LEVEL > 0;         // Condition when module is enabled or not
		//$this->hidden = ! ($user->rights->machinbidule->myobject->read);   // Condition when module is visible by user (test on permission)
	}

	/**
	 * Load data into info_box_contents array to show array later. Called by Dolibarr before displaying the box.
	 *
	 * @param int $max Maximum number of records to load
	 * @return void
	 */
	public function loadBox($max = 5)
	{
		global $langs, $conf;

		// Use configuration value for max lines count
		$this->max = $max;

		//dol_include_once("/machinbidule/class/machinbidule.class.php");

		// Populate the head at runtime
		$text = $langs->trans("Annuaire", $max);

		$this->info_box_head = array(
			'text' => $text,
			'limit'=> dol_strlen($text),
			'graph'=> 1,
			'sublink'=>'',
			'subtext'=>$langs->trans("Filter"),
			'subpicto'=>'filter.png',
			'subclass'=>'linkobject boxfilter',
			'target'=>'none'	// Set '' to get target="_blank"
		);

		if (empty($conf->use_javascript_ajax))
		{
			$langs->load("errors");
			$mesg = $langs->trans("WarningFeatureDisabledWithDisplayOptimizedForBlindNoJs");
		}



		// Populate the contents at runtime
		$this->info_box_contents = array(
			0 => array( // First line
				0 => array( // First Column
					//  HTML properties of the TR element. Only available on the first column.
					'tr' => 'class="left"',
					// HTML properties of the TD element
					'td' => '',

					// Main text for content of cell
					'text' => 'First cell of first line',
					// Link on 'text' and 'logo' elements
					'url' => 'http://example.com',
					// Link's target HTML property
					'target' => '_blank',
					// Fist line logo (deprecated. Include instead logo html code into text or text2, and set asis property to true to avoid HTML cleaning)
					//'logo' => 'monmodule@monmodule',
					// Unformatted text, added after text. Usefull to add/load javascript code
					'textnoformat' => '',

					// Main text for content of cell (other method)
					//'text2' => '<p><strong>Another text</strong></p>',

					// Truncates 'text' element to the specified character length, 0 = disabled
					'maxlength' => 0,
					// Prevents HTML cleaning (and truncation)
					'asis' => false,
					// Same for 'text2'
					'asis2' => true
				),
			),
			1 => array( // First line
				0 => array( // First Column
					//  HTML properties of the TR element. Only available on the first column.
					'tr' => 'class="left"',
					// HTML properties of the TD element
					'td' => '',

					// Main text for content of cell
					'text' => 'First cell of first line',
					// Link on 'text' and 'logo' elements
					'url' => 'http://example.com',
					// Link's target HTML property
					'target' => '_blank',
					// Fist line logo (deprecated. Include instead logo html code into text or text2, and set asis property to true to avoid HTML cleaning)
					//'logo' => 'monmodule@monmodule',
					// Unformatted text, added after text. Usefull to add/load javascript code
					'textnoformat' => '',

					// Main text for content of cell (other method)
					//'text2' => '<p><strong>Another text</strong></p>',

					// Truncates 'text' element to the specified character length, 0 = disabled
					'maxlength' => 0,
					// Prevents HTML cleaning (and truncation)
					'asis' => false,
					// Same for 'text2'
					'asis2' => true
				),
			),
		);
		$options = array();
		$selectList = explode("\n", $conf->global->MACHINBIDULE_URLFORGEID);
		foreach ($selectList as $item) {
			list($forgeId, $label) = explode(',', $item);
			$options[$forgeId]= $label;
		}
		$forgeId = GETPOST("forgeId", 'alpha');
		if (empty($forgeId)) {
			$forgeId = key(($options));
		}
		$legend[] = $options[$forgeId];
		$data1 = array();
		$url= $conf->global->MACHINBIDULE_URLFORGE;
		$url = str_replace('{FORGEID}', $forgeId, $url);
		$json= json_decode(@file_get_contents($url));
		if (empty($json)){
			$mesg= "Impossible de récupérer les données ! ";
		}else{
			foreach ($json->result as $key => $value) {
				$data1[] = array($key, $value);
			}

		}
		//var_dump($data1);
		//die();
		$dir = "";
		$prefix = "";
		$filenamenb = $dir."/".$prefix."annuaire.png";
		$filenamenb2 = $dir."/".$prefix."annuaire2.png";
		$px1 = new DolGraph();
		$px2 = new DolGraph();
		$mesg = $px1->isGraphKo();
		if (!$mesg)
		{
			$px1->SetData($data1);
			$legend = array("Nombre");
			$title =  $options[$forgeId];
			$px1->SetLegend($legend);
			$px1->SetMaxValue($px1->GetCeilMaxValue());
			$px1->SetWidth(500);
			$px1->SetHeight(400);
			$px1->SetShading(3);
			$px1->SetHorizTickIncrement(1);
			$px1->SetCssPrefix("cssboxes");
			$px1->mode = 'depth';
			$px1->SetTitle($title);
			$px1->draw($filenamenb);

			$px2->SetLegend($legend);
			$px2->setShowPercent(1);
			$px2->SetType(array('pie'));
			$px2->SetData(array_slice($data1,0,10));
			$px2->SetMaxValue($px2->GetCeilMaxValue());
			$px2->SetMinValue(500);
			$px2->SetWidth(500);
			$px2->SetHeight(192);
			$px2->SetShading(3);
			$px2->SetHorizTickIncrement(1);
			$px2->SetCssPrefix("cssboxes");
			$px2->mode = 'depth';
			$px2->SetTitle($title);


		//	$px2->setShowLegend(2);
			$px2->draw($filenamenb2);
			$refreshaction = 'refresh_'.$this->boxcode;
			$stringtoshow = '';
			$stringtoshow .= '<script type="text/javascript" language="javascript">
				jQuery(document).ready(function() {
					jQuery("#idsubimg'.$this->boxcode.'").click(function() {
						jQuery("#idfilter'.$this->boxcode.'").toggle();
					});
				});
				</script>';
			$stringtoshow .= '<div class="center hideobject" id="idfilter'.$this->boxcode.'">'; // hideobject is to start hidden
			$stringtoshow .= '<form class="flat formboxfilter" method="POST" action="'.$_SERVER["PHP_SELF"].'">';
			$stringtoshow .= '<input type="hidden" name="token" value="'.newToken().'">';
			$stringtoshow .= '<select name="forgeId">';
			foreach ($selectList as $item) {
				if (empty($item)) {
					continue;
				}
				list($value, $label) = explode(",", $item);
				$stringtoshow .= '<option value="'.$value.'"';
				if ($forgeId==$value){
					$stringtoshow .= ' selected=selected';
				}
				$stringtoshow .='>'.$label.'</option>';
			}
			$stringtoshow .= ' </select>';
			$stringtoshow .= '<input type="hidden" name="action" value="'.$refreshaction.'">';
			$stringtoshow .= '<input type="hidden" name="page_y" value="">';
		//	$stringtoshow .= '<input type="hidden" name="DOL_AUTOSET_COOKIE" value="DOLUSERCOOKIE_box_'.$this->boxcode.':year,showM,showtot">';
			//$stringtoshow .= '<input type="checkbox" name="'.$param_showM.'"'.($showM ? ' checked' : '').'> '.$langs->trans("NumberOfOrdersByMonth");
		//	$stringtoshow .= ' &nbsp; ';
		//	$stringtoshow .= '<input type="checkbox" name="'.$param_showtot.'"'.($showtot ? ' checked' : '').'> '.$langs->trans("AmountOfOrdersByMonthHT");
			$stringtoshow .= '<br>';
		//	$stringtoshow .= $langs->trans("Year").' <input class="flat" size="4" type="text" name="'.$param_year.'" value="'.$endyear.'">';
			$stringtoshow .= '<input type="image" class="reposition inline-block valigntextbottom" alt="'.$langs->trans("Refresh").'" src="'.img_picto($langs->trans("Refresh"), 'refresh.png', '', '', 1).'">';
			$stringtoshow .= '</form>';
			$stringtoshow .= '</div>';
			$stringtoshow .= $px1->show();


		} else {
			var_dump($mesg);
			$this->info_box_contents[0][0] = array(
				'tr'=>'class="oddeven nohover"',
				'td' => 'class="nohover left"',
				'maxlength'=>500,
				'text' => $mesg,
			);
		}
		$this->info_box_contents[0][0] = array(
			'tr'=>'class="oddeven nohover"',
			'td' => 'class="nohover center"',
			'textnoformat'=>$stringtoshow,
		);
		//var_dump($data1);
		$this->info_box_contents[1][0] = array(
			'tr'=>'class="oddeven nohover"',
			'td' => 'class="nohover center"',
			'textnoformat'=>$px2->show(),
		);

	}

	/**
	 * Method to show box. Called by Dolibarr eatch time it wants to display the box.
	 *
	 * @param array $head       Array with properties of box title
	 * @param array $contents   Array with properties of box lines
	 * @param int   $nooutput   No print, only return string
	 * @return void
	 */
	public function showBox($head = null, $contents = null, $nooutput = 0)
	{
		// You may make your own code here…
		// … or use the parent's class function using the provided head and contents templates
		return parent::showBox($this->info_box_head, $this->info_box_contents, $nooutput);
	}
}
